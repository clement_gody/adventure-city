# Adventure City

Ce projet avait pour but de concevoir un site de e-commerce avec 
certaines fonctionnalités obligatoires : 

Le site Web aura la structure de navigation suivante :
-	Il doit avoir un en-tête en haut qui contient le nom et le logo de l'application. Elle doit avoir un menu principal et l'application doit contenir un minimum de 5 pages Web (HTML ou JSP) qui peuvent varier selon que vous êtes un utilisateur connecté ou non. Par exemple, Accueil, Catalogue, Inscription, Contact, etc.
-	Il y aura au moins deux types d'utilisateurs qui doivent chacun être en mesure d'effectuer au minimum les fonctionnalités suivantes :

### Utilisateur

-	Enregistrez ou modifiez vos informations : un utilisateur peut se connecter pour obtenir toutes les fonctionnalités de la page ou s'il est un utilisateur non enregistré, il peut s'inscrire et consulter le catalogue.
-	Voir le catalogue de produits : Que vous soyez un utilisateur enregistré ou que vous puissiez parcourir pour voir la gamme de produits disponibles dans le magasin.
-	Faire un achat : Si vous êtes un utilisateur enregistré lors de la consultation du catalogue, vous aurez des fonctionnalités supplémentaires pour ajouter des produits au panier.
-	Accédez à l'historique des achats : Si vous êtes un utilisateur enregistré, vous aurez vos achats précédents pour votre commodité afin d'accélérer les achats.

### Administrateur

-	Voir les produits les plus achetés : l'administrateur pourra voir une liste des produits les plus achetés au cours d'une certaine période.
-	Voir les jours avec plus d’achats : L'administrateur pourra voir quels jours auront le plus d'achats afin d'obtenir des données d'études de marché.
-	Faire des offres sur les produits : l'administrateur peut faire différents types d'offres ou de remises sur différents produits.
-	Enregistrer ou modifier des produits : l'administrateur peut ajouter ou supprimer des produits du catalogue ou modifier l'une des caractéristiques du produit, que ce soit le prix, la description ou l'offre, entre autres.

## Les technologies utilisées

Dans un contexte de mise en pratique d'un cours, le choix des technologies nous étaients imposés :
- HTML
- CSS
- JavaScript
- Servlets
- JSP
- JSTL
- Architecture MVC
- Gestion des sessions
- Gestion des cookies
- MySQL
- Apache Tomcat


## Le site Adventure City

Pour répondre aux contraintes et fonctionnalités j'ai décidé de développer un site dans le secteur du tourisme : une site internet qui permet d'acheter des tickets 
pour des activités à faire dans les villes.


